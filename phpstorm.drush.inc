<?php

/**
 *  * @file
 *   *
 *    * Generates a command XML file for PHPStorm/WebStorm/IntelliJ IDEs
 *     * that provides integration with Drush
 *      */

/**
 *  * Implementation of hook_drush_command().
 *   */
function phpstorm_drush_command() {
    $items['generate-phpstorm-commands'] = array(
          'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
              'description' => 'Generates an XML file that describes drush commands to PHPStorm and other Jetbrains IDEs',
                );
      return $items;
}

/**
 *  * Drush callback; generate the XML
 *   */
function drush_phpstorm_generate_phpstorm_commands() {
    print '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
      print '<framework xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="schemas/frameworkDescriptionVersion1.1.xsd" name="Drush" invoke="drush" alias="drush" enabled="true" version = "1">' . "\n";

  $commands = drush_get_commands();
  $filtered_commands = array();
  foreach ($commands as $command => $details) {
    if (!$details['hidden']) {
      $filtered_commands[$details['command']] = $details;
    }
  }

  foreach ($filtered_commands as $command => $details) {
    print '  <command>' . "\n";
    print '    <name>' . $command . '</name>' . "\n";
    print '    <help>' . $details['description'] . '</help>' . "\n";
    $params_string = '    <params>';

    foreach ($details['arguments'] as $param => $param_help) {
      $params_string .= $param;
      if (substr($param_help, 0, 8) == 'Optional') {
        $params_string .= "[=null]";
      }
      $params_string .= " ";
    }
    $params_string = substr($params_string, 0, -1) . '</params>';

    if ($params_string != '    <params</params>') {
      print $params_string . "\n";
    }

    print '  </command>' . "\n";
  }

  print '</framework>' . "\n";
}

